'use strict'
const AWS = require('aws-sdk');

AWS.config.update({ region: "eu-west-1" });

exports.handler = (event, context, callback) => {
    const ddb = new AWS.DynamoDB({ apiVersion: "2012-10-08" });


    const { _id,
        username,
        validFrom, 
        expiration, 
        rfid, 
        mac, 
        action, 
        turnstilesActive, 
        result, 
        visitor, 
        company, 
        tenant, 
        passepartout, 
        timestamp } =  JSON.parse(event.body);
    
    
    const params = {
        TableName: "Logs",
        Item: {
            id:{
             S: _id   
            },
            username: {
                S: username
            },
            validFrom: {
                S: validFrom
            },
            expiration: {
                S:expiration
            },
            timestamp: {
                S: timestamp
            },
            visitor: {
                BOOL: visitor
            },
            company: {
                S: company
            },
            action: {
                S: action
            },
            rfid: {
                S: rfid
            },
            mac: {
                S: mac
            },
            tenant: {
                S: tenant
            },
            turnstilesActive: {
                BOOL: turnstilesActive
            },
            result: {
                BOOL: result
            },
            passepartout: {
                BOOL: passepartout
            }
        }
    };

    ddb.putItem(params, (err, data) => {
        if(err) {
            return "Error:" + err;
        } else {
            return "Success:" + data;
        }
    });
}