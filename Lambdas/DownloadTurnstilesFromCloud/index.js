'use strict'
const AWS = require('aws-sdk');

exports.handler = (event, context, callback) => {
    const docClient = new AWS.DynamoDB.DocumentClient({region: 'eu-west-1'});


    const params = {
        TableName: "Turnstiles"
    };

    const response = {
        statusCode: 200,
        body: ""
    }

    docClient.scan(params,  (err, data) => {
        if (err) {
            callback(Error(err));
        } else {
            response.body = JSON.stringify(data.Items);
            callback(null, response);
        }
    });
}