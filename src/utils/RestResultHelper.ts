'use strict';
import { Response } from 'express';

export function _r<T>(res: Response, code: ResultCodes, data?: T | string) {
    return res.contentType('application/json').status(code).send(data);
}

export enum ResultCodes {
    Ok = 200,
    Created = 201,
    NoContent = 204,
    Conflict = 409,
    NotFound = 404,
    Unprocessable = 422,
    Error = 500,
}

export interface RestResult {
    statusCode: ResultCodes;
    body: string;
    headers: any;
}

export class CustomResponse {
    resultCode: ResultCodes;
    message: string;
    constructor() {
        this.resultCode = 400;
        this.message = "Bad Request";
    }
}