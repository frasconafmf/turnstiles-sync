import { Application } from 'express';
import { SyncController } from './controllers/sync.controller';
import { LogController} from './controllers/log.controller';

export class Router {
    private syncController: SyncController
    private logController: LogController

    constructor(private app: Application) {
        this.logController = new LogController();
        this.syncController = new SyncController();
        this.routes();
    }
    
    public routes () {

        // return a welcome message
        this.app
            .route('/')
            .get(this.syncController.welcomeMessage);
        
        // upload logs to Cloud
        this.app
            .route('/upload-logs-cloud/:from/:to')              // The format must be YYYY-MM-DDTHH-MM-SSZ where T and Z are constants
            .get(this.syncController.uploadLogsToCloud);
        
        // download badges and turnstiles from cloud
        this.app
            .route('/download-badges-turnstiles')
            .get(this.syncController.downloadBadgesTurnstilesFromCloud);

        this.app.use((req, res) => res.status(404).send("Not found"));
    };

}