import * as dotenv from 'dotenv';
import express, { Application, Request, Response } from 'express';
import mongoose from 'mongoose';
import { Router } from './router';

dotenv.config();

class App {
    public app: Application;
    public router: Router;

    constructor() {
        this.app = express();
    
        this.setMongoConfig();
        this.router = new Router(this.app);
      }

      private setMongoConfig() {
        mongoose.Promise = global.Promise;
        mongoose.set('useFindAndModify', false);
        mongoose.set('useUnifiedTopology', true);
        mongoose.set('useCreateIndex', true);
        mongoose.connect(process.env.MONGO_URL || "", {
          useNewUrlParser: true
        }, (error) => {
          if(error) console.log(error);
          else console.log(`Connected with MongoDB`);
        });
      }
}

export default new App().app;