import { Request, Response, response } from 'express';
import { _r, CustomResponse, ResultCodes } from "../utils/RestResultHelper";
import { Log, ILog } from '../model/log.model';
import { Badge, IBadge } from '../model/badge.model';
import { MongooseDocument } from 'mongoose';
import { isEmptyObject } from '../utils/isEmptyObject';
import axios from 'axios';
import { Turnstile , ITurnstile } from '../model/turnstiles.model';


export class SyncController {
    private readonly defaultResponse: CustomResponse;

    constructor() {
        this.defaultResponse = new CustomResponse();
    }

    public welcomeMessage(req: Request, res: Response) {
        return res.status(200).json(process.env.WELCOME_MESSAGE);
    }

    public uploadLogsToCloud = async (req: Request, res: Response) => {

        const from: Date = new Date(req.params.from);
        const to: Date = new Date(req.params.to);

        let customResponse: CustomResponse = this.defaultResponse;
        // upload the logs that have false in uploaded
        await Log.find({
            timestamp: {
                "$gte": from,
                "$lt": to
            }, uploaded: false
        }).exec(async (error: Error, logs: MongooseDocument) => {
            if (error) {
                customResponse.resultCode = ResultCodes.Error;
                customResponse.message = error.message;
            } else {
                customResponse.resultCode = ResultCodes.Ok;
                customResponse.message = JSON.stringify(logs);
                const data = JSON.parse(customResponse.message)

                const options = {
                    headers: { "x-api-key": process.env.API_KEY_GATEWAY }
                }

                if (isEmptyObject(<Object>data)) {
                    customResponse.message = "No logs to upload.";
                    console.log("No logs to upload.");

                } else
                    console.log(`uploading logs...`);

                data.forEach((element: ILog) => {

                    axios.post(process.env.URL_API_GATEWAY + (process.env.UPLOAD_LOGS_ENDPOINT || '') || '', { "body": JSON.stringify(element) }, options)
                        .then(response => {
                            console.log(`status: ${response.status}`);
                        })
                        .catch(error => {
                            console.log(`error: ${error}`);
                        });

                });
                // Set uploaded to true
                if (!(customResponse.message === "No logs to upload."))
                    await Log.updateMany({ "uploaded": false }, { "$set": { "uploaded": true } }, { "multi": true }, (err, writeResult) => { });
                return _r<ILog[]>(res, customResponse.resultCode, customResponse.message);
            }
        });
    }

    public downloadBadgesTurnstilesFromCloud = async (req: Request, res: Response) => {

        let customResponse: CustomResponse = this.defaultResponse;
        let badges, turnstiles;
        const options = {
            headers: { "x-api-key": process.env.API_KEY_GATEWAY }
        }

        await axios.get(process.env.URL_API_GATEWAY + (process.env.DOWNLOAD_BADGES_ENDPOINT || '') || '', options)
            .then(response => {
                customResponse.resultCode = ResultCodes.Ok;
                badges = JSON.parse(JSON.parse( JSON.stringify(response.data.body)));

                badges.forEach(async (element: IBadge) => {
                    await Badge.findOne({ rfid: element.rfid, tenant: element.tenant })
                        .exec(async (error: Error, badge: MongooseDocument) => {
                            if (error) {
                                console.log(error);

                            } else if (badge === null) {
                                await Badge.create(element, (err: Error) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log("Badge created!");
                                    }
                                });
                            } else {
                                await Badge.updateOne({ rfid: element.rfid, tenant: element.tenant }, element, (err: Error) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log("Badge updated");
                                    }
                                })
                            }
                        });
                });
                customResponse.message = "Badge Downloaded\n";
            })
            .catch(error => {
                customResponse.resultCode = ResultCodes.Error;
                customResponse.message = error;
                console.log(`error: ${error}`);
            });

        await axios.get(process.env.URL_API_GATEWAY + (process.env.DOWNLOAD_TURNSTILES_ENDPOINT || '') || '', options)
            .then(response => {
                turnstiles = JSON.parse(JSON.parse(JSON.stringify(response.data.body)));

                turnstiles.forEach(async (element: ITurnstile) => {
                    await Turnstile.findOne({ mac: element.mac })
                        .exec(async (error: Error, badge: MongooseDocument) => {
                            if (error) {
                                console.log(error);

                            } else if (badge === null) {
                                await Turnstile.create(element, (err: Error) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log("Turnstile created!");
                                    }
                                });
                            } else {
                                await Turnstile.updateOne({ mac: element.mac }, element, (err: Error) => {
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        console.log("Turnstile updated");
                                    }
                                })
                            }
                        });
                });
                customResponse.message += "Turnstiles Downloaded\n";
            })
            .catch(error => {
                customResponse.resultCode = ResultCodes.Error;
                customResponse.message = error;
                console.log(`error: ${error}`);
            });
            
        return _r<IBadge>(res, customResponse.resultCode, customResponse.message);
    }

}