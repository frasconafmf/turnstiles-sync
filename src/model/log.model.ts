import mongoose from 'mongoose';

const LogSchema = new mongoose.Schema({
    "rfid": {
      "type": "String"
    },
    "mac": {
      "type": "String"
    },
    "action": {
      "type": "String"
    },
    "turnstilesActive": {
      "type": "Boolean"
    },
    "result": {
      "type": "Boolean"
    },
    "username": {
      "type": "String"
    },
    "visitor": {
      "type": "Boolean"
    },
    "company": {
      "type": "String"
    },
    "tenant": {
      "type": "String"
    },
    "validFrom": {
      "$date": {
        "$numberLong": {
          "type": "String"
        }
      }
    },
    "expiration": {
      "$date": {
        "$numberLong": {
          "type": "String"
        }
      }
    },
    "passepartout": {
      "type": "Boolean"
    },
    "timestamp": {
      "$date": {
        "$numberLong": {
          "type": "String"
        }
      }
    },
    "uploaded": {
      "type": "Boolean"
    }
  });


export const Log = mongoose.model("logs", LogSchema);


export interface ILog {
  _id : string
  rfid: string;
  mac: string;
  action: string;
  turnstilesActive: boolean;
  result: boolean;
  username: string;
  visitor: boolean;
  company: string;
  tenant: string;
  validFrom: ValidFrom;
  expiration: Expiration;
  passepartout: boolean;
  timestamp: Timestamp;
  uploaded: boolean;
}

export interface ValidFrom {
  $date: Date;
}

export interface Date {
  $numberLong: string;
}

export interface Expiration {
  $date: Date2;
}

export interface Date2 {
  $numberLong: string;
}

export interface Timestamp {
  $date: Date3;
}

export interface Date3 {
  $numberLong: string;
}


