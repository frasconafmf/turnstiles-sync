import mongoose from 'mongoose';

const TurnstileSchema = new mongoose.Schema({
    "name": {
      "type": "String"
    },
    "ip": {
      "type": "String"
    },
    "mac": {
      "type": "String"
    },
    "action": {
      "type": "String"
    },
    "active": {
      "type": "Boolean"
    },
    "permission": {
      "allow": {
        "username": {
          "type": [
            "String"
          ]
        },
        "company": {
          "type": [
            "String"
          ]
        },
        "rfid": {
          "type": [
            "String"
          ]
        },
        "visitor": {
          "type": "Boolean"
        }
      },
      "deny": {
        "username": {
          "type": [
            "String"
          ]
        },
        "company": {
          "type": [
            "String"
          ]
        },
        "rfid": {
          "type": [
            "String"
          ]
        },
        "visitor": {
          "type": "Boolean"
        }
      }
    }
  });

export const Turnstile = mongoose.model("turnstiles", TurnstileSchema);

export interface ITurnstile {
    _id: Id;
    name: string;
    ip: string;
    mac: string;
    action: string;
    active: boolean;
    permission: Permission;
  }
  
  export interface Id {
    $oid: string;
  }
  
  export interface Permission {
    allow: Allow;
    deny: Deny;
  }
  
  export interface Allow {
    username: string[];
    company: string[];
    rfid: string[];
    visitor: boolean;
  }
  
  export interface Deny {
    username: string[];
    company: string[];
    rfid: string[];
    visitor: boolean;
  }