import mongoose from 'mongoose';

const BadgeSchema = new mongoose.Schema({
    "rfid": {
      "type": "String"
    },
    "active": {
      "type": "Boolean"
    },
    "tenant": {
      "type": "String"
    },
    "username": {
      "type": "String"
    },
    "company": {
      "type": "String"
    },
    "visitor": {
      "type": "Boolean"
    },
    "passepartout": {
      "type": "Boolean"
    },
    "validFrom": Date,
    "expiration": Date,
  });

export const Badge = mongoose.model("badges", BadgeSchema);

export interface IBadge {
    _id: Id;
    rfid: string;
    active: boolean;
    tenant: string;
    username: string;
    company: string;
    visitor: boolean;
    passepartout: boolean;
    validFrom: ValidFrom;
    expiration: Expiration;
  }
  
  export interface Id {
    $oid: string;
  }
  
  export interface ValidFrom {
    $date: Date;
  }
  
  export interface Date {
    $numberLong: string;
  }
  
  export interface Expiration {
    $date: Date2;
  }
  
  export interface Date2 {
    $numberLong: string;
  }